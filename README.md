# Assumptions

- When saving data returned from the combine operation, one row will be saved per hotel returned in the combined data
- Data is normalized and stored in the hotels table as a key-value data structure in which the key is the "public_id/entity_id" of the hotel and the value is the JSON from the latest API combined with the retail price from the legacy API
- All values except price are the same between the two APIs (hence why we can take just the JSON from the latest API and append prices)

- If an entry already exists in the DB, only update the JSON data associated with it
  - Do not add a new entry 

Run using the following command:

`python3.6 main.py citystring checkinstring checkoutString`

Note: Requires python3.6 otherwise you might get an error importing the requests module