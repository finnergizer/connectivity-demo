import sys
from data_fetch_service import HotelsApiService
from hotel_data_helper import HotelDataHelper

try:
    city = sys.argv[1]
    checkin = sys.argv[2]
    checkout = sys.argv[3]
except IndexError:
    city = "New York"
    checkin = "2019-10-20"
    checkout = "2019-10-22"

hotel_api_service = HotelsApiService()
hotel_data_helper = HotelDataHelper()

hotels_data = hotel_api_service.get_hotels_data(city, checkin, checkout)
legacy_hotels_data = hotel_api_service.get_legacy_hotels_data(city, checkin, checkout)

combined_data = hotel_data_helper.combine(hotels_data, legacy_hotels_data)

hotel_data_helper.save(combined_data)

hotel_data_helper.cleanup()


