import json
import sqlite3

class HotelDataHelper:
    def __init__(self):
        self.connection = sqlite3.connect('combined.db')
        self.cursor = self.connection.cursor()
        self.cursor.execute(
            '''
            CREATE TABLE IF NOT EXISTS hotels (public_id INTEGER, json_data TEXT)
            '''
        )

    def combine(self, hotel_data, legacy_hotel_data):
        combined_data = []
        for hotel in hotel_data.get("hotels", []):
            matching_hotel = legacy_hotel_data.find("./element/[id='{}']".format(hotel["id"]))
            if matching_hotel != None:
                hotel["prices"] = {"snaptravel": hotel["price"], "retail":matching_hotel.findtext("./price")}
                combined_data.append(hotel)

        return combined_data

    def save(self, hotel_data):
        for hotel in hotel_data:
            try:
                self.cursor.execute("SELECT * FROM hotels WHERE public_id = ?", (hotel["id"], ))
                if len(self.cursor.fetchall()):
                    self.cursor.execute("UPDATE hotels SET json_data = ? WHERE public_id = ?", (json.dumps(hotel), hotel["id"]))
                else:
                    self.cursor.execute("INSERT into hotels (public_id, json_data) VALUES(?, ?)",
                                    (hotel["id"], json.dumps(hotel)))
            except sqlite3.Error as error:
                print("Failed to insert data into sqlite table", error)
        self.connection.commit()

    def cleanup(self):
        self.cursor.close()
        self.connection.close()