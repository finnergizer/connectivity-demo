import json
import requests
from xml.etree import ElementTree

class HotelsApiService:

    hotels_url = "https://experimentation.getsnaptravel.com/interview/hotels"
    hotels_headers = {'Content-Type': 'application/json'}

    legacy_hotels_url = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"
    legacy_hotels_headers = {'Content-Type': 'application/xml'}

    # def __init__(self, city, check_in, check_out):
    #     self.city = city
    #     self.check_in = check_in
    #     self.check_out = check_out
    #
    def get_hotels_data(self, city, check_in, check_out):
        hotels_json_payload = {
            "city": city,
            "checkin": check_in,
            "checkout": check_out,
            "provider": "snaptravel"
        }

        request = requests.post(url=self.hotels_url, data=json.dumps(hotels_json_payload), headers=self.hotels_headers)
        request_data = request.json()
        return request_data

    def get_legacy_hotels_data(self, city, check_in, check_out):
        hotels_legacy_xml_payload = """
        <?xml version="1.0" encoding="UTF-8"?>
        <root>
          <checkin>{}</checkin>
          <checkout>{}</checkout>
          <city>{}</city>
          <provider>snaptravel</provider>
        </root>
        """.format(check_in, check_out, city)

        legacy_request = requests.post(url=self.legacy_hotels_url, data=hotels_legacy_xml_payload,
                                       headers=self.legacy_hotels_headers)
        legacy_request_data = ElementTree.fromstring(legacy_request.content)

        return legacy_request_data
